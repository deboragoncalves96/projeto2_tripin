﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tripin.DAL;
using tripin.Models;

namespace tripin.Controllers
{
    public class RotasController : Controller
    {
        private TripinDbContext db = new TripinDbContext();

        // GET: Rotas
        public ActionResult Index()
        {
            return View(db.Rotas.ToList());
        }

        // GET: Rotas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rota rota = db.Rotas.Find(id);
            if (rota == null)
            {
                return HttpNotFound();
            }
            return View(rota);
        }

        // GET: Rotas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rotas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RotaID,nome_rota,lat1_rota,lat2_rota,lat3_rota,lat4_rota,lat5_rota,lon1_rota,lon2_rota,lon3_rota,lon4_rota,lon5_rota")] Rota rota)
        {
            if (ModelState.IsValid)
            {
                db.Rotas.Add(rota);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rota);
        }

        // GET: Rotas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rota rota = db.Rotas.Find(id);
            if (rota == null)
            {
                return HttpNotFound();
            }
            return View(rota);
        }

        // POST: Rotas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RotaID,nome_rota,lat1_rota,lat2_rota,lat3_rota,lat4_rota,lat5_rota,lon1_rota,lon2_rota,lon3_rota,lon4_rota,lon5_rota")] Rota rota)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rota).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rota);
        }

        // GET: Rotas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rota rota = db.Rotas.Find(id);
            if (rota == null)
            {
                return HttpNotFound();
            }
            return View(rota);
        }

        // POST: Rotas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rota rota = db.Rotas.Find(id);
            db.Rotas.Remove(rota);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
