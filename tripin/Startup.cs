﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(tripin.Startup))]
namespace tripin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
