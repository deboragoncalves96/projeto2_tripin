namespace TripIn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class floatponto : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pontoes", "latitude_ponto", c => c.Single(nullable: false));
            AlterColumn("dbo.Pontoes", "longitude_ponto", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pontoes", "longitude_ponto", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Pontoes", "latitude_ponto", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
