namespace TripIn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class doubleponto : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pontoes", "latitude_ponto", c => c.Double(nullable: false));
            AlterColumn("dbo.Pontoes", "longitude_ponto", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pontoes", "longitude_ponto", c => c.Single(nullable: false));
            AlterColumn("dbo.Pontoes", "latitude_ponto", c => c.Single(nullable: false));
        }
    }
}
