namespace TripIn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _decimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pontoes", "latitude_ponto", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Pontoes", "longitude_ponto", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lat1_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lat2_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lat3_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lat4_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lat5_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lon1_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lon2_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lon3_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lon4_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Rotas", "lon5_rota", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Rotas", "lon5_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lon4_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lon3_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lon2_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lon1_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lat5_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lat4_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lat3_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lat2_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Rotas", "lat1_rota", c => c.Double(nullable: false));
            AlterColumn("dbo.Pontoes", "longitude_ponto", c => c.Double(nullable: false));
            AlterColumn("dbo.Pontoes", "latitude_ponto", c => c.Double(nullable: false));
        }
    }
}
