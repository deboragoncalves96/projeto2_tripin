namespace tripin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pontodecimal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pontoes",
                c => new
                    {
                        PontoID = c.Int(nullable: false, identity: true),
                        nome_ponto = c.String(),
                        tipo_ponto = c.String(),
                        descricao_ponto = c.String(),
                        latitude_ponto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        longitude_ponto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        localidade = c.String(),
                        url = c.String(),
                    })
                .PrimaryKey(t => t.PontoID);
            
            CreateTable(
                "dbo.Rotas",
                c => new
                    {
                        RotaID = c.Int(nullable: false, identity: true),
                        nome_rota = c.String(),
                        lat1_rota = c.Double(nullable: false),
                        lat2_rota = c.Double(nullable: false),
                        lat3_rota = c.Double(nullable: false),
                        lat4_rota = c.Double(nullable: false),
                        lat5_rota = c.Double(nullable: false),
                        lon1_rota = c.Double(nullable: false),
                        lon2_rota = c.Double(nullable: false),
                        lon3_rota = c.Double(nullable: false),
                        lon4_rota = c.Double(nullable: false),
                        lon5_rota = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.RotaID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Rotas");
            DropTable("dbo.Pontoes");
        }
    }
}
