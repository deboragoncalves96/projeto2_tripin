﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using tripin.Models;

namespace tripin.DAL
{
    public class TripinDbContext : DbContext
    {
        public TripinDbContext() : base("DefaultConnection") { }

        public DbSet<Ponto> Pontos { get; set; }
        public DbSet<Rota> Rotas { get; set; }
    }
}