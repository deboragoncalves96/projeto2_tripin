﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TripIn.Models;

namespace TripIn.DAL
{
    public class TripInContext : DbContext
    {
        public TripInContext() : base("DefaultConnection")
        {
        }

        public DbSet<Rota> Rotas { get; set; }
        public DbSet<Ponto> Pontos { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ponto>().Property(x => x.latitude_ponto);
            modelBuilder.Entity<Ponto>().Property(x => x.longitude_ponto);
        }
    }
}